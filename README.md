

# Log Java RMI


## Dependencias

-   Java 8 o superior
-   Maven


## Estructura de Directorios

    ├── src
    │   ├── main
    │   │   └── java
    │   │       └── ucab
    │   │           └── ds
    │   │               └── log
    |   |                   (...)
    │   │                   ├── client
    │   │                   ├── server
    │   │                   └── shared
    │   └── (...)

-   `client`: El directorio contiene los archivos fuente necesarios para
    correr la aplicacion cliente
-   `server`: El directorio contiene los archivos fuente necesarios para
    correr la aplicacion servidor
-   `shared`: El directorio contiene los archivos fuente que vinculan al
    cliente con el servidor (stubs, interfaces compartidas)

