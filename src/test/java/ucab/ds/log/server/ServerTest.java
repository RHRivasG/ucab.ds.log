package ucab.ds.log.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import static junit.framework.Assert.assertTrue;

import org.junit.Test;
import ucab.ds.log.shared.LogStatus;
import ucab.ds.log.shared.Logger;

public class ServerTest {
	@Test
	public void itLogs() {
		try {
			Server.run();
        	Logger logger = (Logger) Naming.lookup("//localhost/MyServer");
        	LogStatus response = logger.log("pruebita");
			assertTrue(response.isSuccess());
		}
        catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Exception");
			e.printStackTrace();
		}
	}
}
 
