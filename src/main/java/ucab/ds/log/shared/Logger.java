package ucab.ds.log.shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Logger extends Remote {
	public LogStatus log(String text) throws RemoteException;
}
