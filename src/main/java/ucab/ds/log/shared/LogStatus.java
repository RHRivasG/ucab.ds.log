package ucab.ds.log.shared;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class LogStatus implements Serializable {
    private String message;
    private int status;
    private LocalDateTime logTime;
    private InetAddress sender;
    private InetAddress host;

    private LogStatus(String message, int status, InetAddress sender) {
        this.status = status;
        this.message = message;
        this.sender = sender;

        logTime = LocalDateTime.now();
        try {
            host = InetAddress.getLocalHost();
        } catch(UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public boolean isSuccess() {
        return status == 1;
    }

    public boolean isFailure() {
        return status == 2;
    }

    public String getMessage() {
        return message;
    }

    // SUCCESSFULL LOG
    public static LogStatus success(String message, InetAddress sender) {
        return new LogStatus(message, 1, sender);
    }

    // FAILED LOG
    public static LogStatus failure(String message, InetAddress sender) {
        return new LogStatus(message, 2, sender);
    }
}
