package ucab.ds.log.server;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import ucab.ds.log.shared.Logger;
import ucab.ds.log.shared.LogStatus;

public class Server extends UnicastRemoteObject implements Logger {

	protected Server() throws RemoteException{
		super();
	}

	@Override
	public LogStatus log(String text) throws RemoteException{
		try {
			FileWriter myWriter = new FileWriter("log.txt", true);
			BufferedWriter bw = new BufferedWriter(myWriter);
			bw.write(text+"\n");
			bw.close();
			System.out.println("Successfully wrote to the file.");
			String sender = RemoteServer.getClientHost();
			LogStatus status = LogStatus.success(text, InetAddress.getByName(sender));
			return status;
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
			LogStatus status = LogStatus.failure(text, null);
			return status;
		} catch (ServerNotActiveException se) {
			System.out.println("An error occurred.");
			se.printStackTrace();
			return null;
		}
	}

	public static void run(){
		try{
			Server server = new Server();
			Naming.rebind("//localhost/MyServer", server);
			System.out.println("Server ready");
		} catch (Exception e){
			System.err.println("Server exception: " + e.toString());
			e.printStackTrace();
		}
	}

}
