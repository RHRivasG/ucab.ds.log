package ucab.ds.log;

// import java.util.function.Function;
import java.util.function.Consumer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Consumer<String> print = System.out::println;
        print.accept("Hello World");
    }
}
